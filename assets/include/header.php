<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<script src="/assets/js/jquery-3.3.1.min.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPJjSzQWD53iX5qWDzzpR9ZRpZpmwt2tY&callback=initMap"
  type="text/javascript"></script>
<script>
    function initMap() {
           var latLng = new google.maps.LatLng(36.035358,  139.402245);
        if ($(window).width() > 767) {
           var minZoomLevel = 17;
       } else{
           var minZoomLevel = 16;
       }
        var map = new google.maps.Map(
            document.getElementById("map"),
            {
                zoom: minZoomLevel,
                center: latLng,
                scrollwheel: false
              }
        );
        var marker = new google.maps.Marker({
              position: latLng,
              map: map,
              title: "marker"
        });
    }
</script>
</head>
<body class="page-<?php echo $id; ?>">
	<header id="header" class="c-header">
		<div class="c-header1">
			<img src="/assets/image/common/img-header1.png" alt="" class="pc-only">
			<img src="/assets/image/common/img-header1sp.png" alt="" class="sp-only">
		</div>
		<div class="c-header2">
			<img src="/assets/image/common/img-header2.png" alt="" class="pc-only">
			<img src="/assets/image/common/img-header2sp.png" alt="" class="sp-only" width="320" height="101">
		</div>
	</header><!-- /header -->