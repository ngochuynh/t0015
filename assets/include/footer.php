<footer class="c-footer">
	<div class="c-footer1">
		<h2>
			<img src="/assets/image/common/text-info.png" alt="" class="pc-only">
			<img src="/assets/image/common/text-infosp.png" alt="" class="sp-only" width="171" height="28">
			<span>お知らせ</span>
		</h2>
		<dl>
			<a href=""><dt>2018年0月0日</dt>
			<dd>インドアゴルフスクール ARROWS 東松山店 サイト<br class="sp-only">公開しました。</dd></a>
		</dl>
	</div>
	<div class="c-footer2">
		<a href="" title="">
			<img src="/assets/image/common/btn-arrow.png" alt="" class="pc-only">
			<img src="/assets/image/common/btn-arrowsp.png" alt="" class="sp-only" width="240" height="50" >
		</a>
		<p>〒355-0028 埼玉県東松山市箭弓町1-13-16 安福ビル1F　TEL.0493-23-8015<br>
		営業時間：［平日］11:00〜21:00（昼休憩13:00〜14:00）／［土・日・祝］10:00〜19:00　火曜定休<br>
		<span class="pc-only">レッスン・フリー利用可能時間：会員・ビジター問わず1時間（準備時間含む）</span></p>
	</div>
	<div class="c-footer3">
		<p>Copyright (c) Shin Corporation All Rights Reserved.</p>
	</div>
	<a href="" class="c-scroll-top">
		<img src="/assets/image/common/scroll-top.png" alt="" class="pc-only">
		<img src="/assets/image/common/scroll-top.png" alt="" class="sp-only" width="50" height="50">
	</a>
</footer>
<script src="/assets/js/functions.js"></script>
</body>
</html>