/*===================================================
Viewport width fit for Tablet
===================================================*/
var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());

if(_ua.Tablet){
  $("meta[name='viewport']").attr('content', 'width=1100');
}


$(document).on('click','.js-acord', function() {
    var this_obj = $(this);

    this_obj.next().slideToggle(400);
    if(this_obj.hasClass('is-open')){
        this_obj.removeClass('is-open');
    }else{
        this_obj.addClass('is-open');
    }
});

$(function(){
 $('a[href^="#"]').click(function(){
   var speed = 500;
   var href= $(this).attr("href");
   var target = $(href == "#" || href == "" ? 'html' : href);
   var position = target.offset().top;
   $("html, body").animate({scrollTop:position}, speed, "swing");
   return false;
 });
});


$(".c-scroll-top").hide();
         $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.c-scroll-top').fadeIn(500);
            } else {
                $('.c-scroll-top').fadeOut(100);
            }
        });

        $('.c-scroll-top').click(function() {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });