<?php $id="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="p-index">
	<section class="p-index1" id="group">

		<div class="p-index1__campaign">
			<!-- <img src="assets/image/common/img-campaign.png" alt="" class="pc-only"> -->
			<div class="p-index1__campaigntext">
				<h2>CAMPAIGN</h2>
			    <span>
			    	<img src="assets/image/common/img-campaign.png" alt="" class="pc-only">
			    	<img src="assets/image/common/img-campaignsp.png" alt="" class="sp-only" width="17" height="15">
			    	キャンペーン
			    </span>
			</div>
			<!-- <img src="assets/image/common/img-campaignsp.png" alt="" class="" width="143" height="48"> -->
		</div>

		<div class="c-breadcrumb pc-only">
			<ul>
				<li><a href="#tour" title="">
					キャンペーン情報
				</a></li>
				<li><a href="#fee" title="">
					ご利用料金
				</a></li>
				<li><a href="#reservation" title="">
					ご予約
				</a></li>
				<li><a href="#instructor" title="">
					講師紹介
				</a></li>
				<li><a href="#shop" title="">
					店舗のご案内
				</a></li>
			</ul>
		</div>

		<div class="p-index1__tour" id="tour">
			<div class="l-container">
				<div class="c-boxsp">
					<img src="assets/image/common/img-box1sp.png" alt="" class="sp-only">
				</div>
				<ul class="c-list1">
					<li class="pc-only">
						<img src="assets/image/common/img-list1.png" alt="" class="pc-only">
					</li>
					<li>
						<img src="assets/image/common/img-list2.png" alt="" class="pc-only">
						<img src="assets/image/common/img-list2sp.png" alt="" class="sp-only">
					</li>
					<li>
						<img src="assets/image/common/img-list3.png" alt="" class="pc-only">
						<img src="assets/image/common/img-list3sp.png" alt="" class="sp-only">
					</li>
				</ul>
				<div class="c-btn1">
					<a href="#" title="">
						<img src="assets/image/common/img-btn1.png" alt="" class="pc-only">
						<img src="assets/image/common/img-btn1sp.png" alt="" class="sp-only" width="280" height="30" >
					</a>
				</div>
			</div>
		</div>

	</section>

	<section class="p-index2" id="fee">
		<div class="l-container">
			<h2 class="c-title1">
			     <img src="assets/image/common/title-fee.png" alt="" class="pc-only">
				 <img src="assets/image/common/title-fee.png" alt="" class="sp-only" width="189" height="34">
		    </h2>

		    <div class="c-title2">
		    	事前申し込み＜期間限定＞
		    </div>

			<ul class="c-list2">
				<li>
					<img src="assets/image/common/img-fee1.png" alt="" class="pc-only">
					<img src="assets/image/common/img-fee1sp.png" alt="" class="sp-only">
				</li>
				<li>
					<img src="assets/image/common/img-fee2.png" alt="" class="pc-only">
					<img src="assets/image/common/img-fee2sp.png" alt="" class="sp-only">
				</li>
			</ul>


			 <div class="c-title2">
		    	通常料金 ※(税抜)
		    </div>


			<table class="c-table1 pc-only">
				<thead>
					<tr>
						<th></th>
						<th></th>
						<th>会員</th>
						<th>ビジター</th>
						<th>フリー練習費</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>⼊会⾦</td>
						<td></td>
						<td>￥5,000／1回</td>
						<td>−</td>
						<td>−</td>
					</tr>
					<tr>
						<td>レギュラー</td>
						<td>全⽇利⽤可</td>
						<td class="u-bold">￥10,000（税抜）／⽉額</td>
						<td>−</td>
						<td class="u-pink u-bold">無料</td>
					</tr>
					<tr>
						<td>デイタイム</td>
						<td>平⽇昼限定（11:00〜17:00）</td>
						<td class="u-bold">￥7,000（税抜）／⽉額</td>
						<td>−</td>
						<td>時間外有料</td>
					</tr>
					<tr>
						<td>ジュニア</td>
						<td>⽊曜⽇限定 17：00〜18：00<br>
						中学3年生まで</td>
						<td class="u-bold">￥6,000（税抜）／⽉額</td>
						<td>−</td>
						<td class="u-bold">時間外有料</td>
					</tr>
					<tr>
						<td>ホリデイ</td>
						<td>⼟・⽇・祝のみ終⽇利⽤可<br>
						中学3年生まで</td>
						<td class="u-bold">￥8,000（税抜）／⽉額</td>
						<td>−</td>
						<td class="u-bold">時間外有料</td>
					</tr>
					<tr>
						<td>レッスン4</td>
						<td>レッスン4回<br>
						（スタンプカード制／有効期間：3ヶ月）</td>
						<td >−</td>
						<td class="u-bold">￥8,000（税抜）／1セット</td>
						<td class="u-bold">有料</td>
					</tr>
					<tr>
						<td>フリー練習</td>
						<td>空打席があれば利⽤可<br>
						※レッスンは含まず</td>
						<td class="u-bold">
							<span class="u-strike">￥1,000（税抜）／1回</span><br>
							<span class="u-pink">プレオープン期間限定 ￥0</span>
						</td>
						<td class="u-bold">
							<span class="u-strike ">￥1,500（税抜）／1回</span><br>
							<span class="u-pink">プレオープン期間限定 ￥0</span>
						</td>
						<td class="u-bold">有料</td>
					</tr>
					<tr>
						<td>体験レッスン</td>
						<td>全⽇利⽤可</td>
						<td class="u-pink">
						−
						</td>
						<td class="u-bold">
						￥2,000（税抜）／1回
						</td>
						<td class="u-bold">−</td>
					</tr>
				</tbody>
			</table>

			<dl class="c-list3 sp-only">
				<dt class="js-acord"><p>入会金</p></dt>
				<dd>
					<table class="c-table2">
						<tbody>
							<tr>
								<th>⼊会⾦</th>
								<td>￥5,000（税抜）／1回</td>
							</tr>
						</tbody>
					</table>
				</dd>
				<dt class="js-acord"><p>レギュラー</p><span>全⽇利⽤可</span></dt>
				<dd>
					<table class="c-table2">
					    <tbody>
					        <tr>
					            <th>会員</th>
					            <td>￥10,000（税抜）／⽉額</td>
					        </tr>
					        <tr>
					            <th>ビジター</th>
					            <td>-</td>
					        </tr>
					        <tr>
					            <th>フリー練習費</th>
					            <td>無料</td>
					        </tr>
					    </tbody>
					</table>
				</dd>
				<dt class="js-acord"><p>デイタイム </p><span>平⽇昼限定（11:00〜17:00）</span></dt>
				<dd>
					<table class="c-table2">
					    <tbody>
					        <tr>
					            <th>会員</th>
					            <td>￥7,000（税抜）／⽉額</td>
					        </tr>
					        <tr>
					            <th>ビジター</th>
					            <td>-</td>
					        </tr>
					        <tr>
					            <th>フリー練習費</th>
					            <td>時間外有料</td>
					        </tr>
					    </tbody>
					</table>
				</dd>
				<dt class="js-acord"><p>ジュニア </p><span>⽊曜⽇限定 17：00〜18：00<br>
						中学3年生まで</span></dt>
				<dd>
					<table class="c-table2">
					    <tbody>
					        <tr>
					            <th>会員</th>
					            <td>￥6,000（税抜）／⽉額</td>
					        </tr>
					        <tr>
					            <th>ビジター</th>
					            <td>-</td>
					        </tr>
					        <tr>
					            <th>フリー練習費</th>
					            <td>時間外有料</td>
					        </tr>
					    </tbody>
					</table>
				</dd>
				<dt class="js-acord"><p>ホリデイ</p>
					<span>⼟・⽇・祝のみ終⽇利⽤可</span></dt>
				<dd>
					<table class="c-table2">
					    <tbody>
					        <tr>
					            <th>会員</th>
					            <td>￥8,000（税抜）／⽉額</td>
					        </tr>
					        <tr>
					            <th>ビジター</th>
					            <td>-</td>
					        </tr>
					        <tr>
					            <th>フリー練習費</th>
					            <td>時間外有料</td>
					        </tr>
					    </tbody>
					</table>
				</dd>
				<dt class="js-acord"><p>レッスン4 </p>
					<span>レッスン4回（スタンプカード制<br>
						／有効期間：3ヶ月）</span></dt>
				<dd>
					<table class="c-table2">
					    <tbody>
					        <tr>
					            <th>会員</th>
					            <td>-</td>
					        </tr>
					        <tr>
					            <th>ビジター</th>
					            <td>￥8,000（税抜）／1セット</td>
					        </tr>
					        <tr>
					            <th>フリー練習費</th>
					            <td>無料</td>
					        </tr>
					    </tbody>
					</table>
				</dd>
				<dt class="js-acord"><p>フリー練習 </p>
					<span>空打席があれば利⽤可<br>
						※レッスンは含まず</span></dt>
				<dd>
					<table class="c-table2">
					    <tbody>
					        <tr>
					            <th>会員</th>
					            <td class="u-bold">
					                <span class="u-strike">￥1,000（税抜）／1回</span><br><span class="u-pink">プレオープン期間限定 ￥0</span>
					        </tr>
					        <tr>
					            <th>ビジター</th>
					            <td class="u-bold">
					                 <span class="u-strike">￥1,500（税抜）／1回</span><br><span class="u-pink">プレオープン期間限定 ￥0</span>
					            </td>
					        </tr>
					        <tr>
					            <th>フリー練習費</th>
					            <td>無料</td>
					        </tr>
					    </tbody>
					</table>
				</dd>
				<dt class="js-acord"><p>体験レッスン </p>
					<span>全⽇利⽤可</span></dt>
				<dd>
					<table class="c-table2">
					    <tbody>
					        <tr>
					            <th>会員</th>
					            <td>-</td>
					        </tr>
					        <tr>
					            <th>ビジター</th>
					            <td>￥2,000（税抜）／1回</td>
					        </tr>
					        <tr>
					            <th>フリー練習費</th>
					            <td>-</td>
					        </tr>
					    </tbody>
					</table>
				</dd>
			</dl>

			<div class="c-note">
				<span class="c-note__green">レッスン・フリー利用可能時間</span>
				<p>1時間（準備時間含む、会員・ビジター問わず）</p>
				<span class="c-note__blue">学生割引</span>
				<p>上記料金より50％OFF（会員カテゴリのみ適用※フリー練習含まず）</p>
			</div>
		</div>
	</section>

	<section class="p-index3" id="reservation">
		
		<div class="l-container">
			<h2>
				<img src="assets/image/common/title-reservation.png" alt="" class="pc-only">
				<img src="assets/image/common/title-reservationsp.png" alt="" class="sp-only" width="256" height="107">
			</h2>
			<p>当スクールは予約制となっております。<br>
				お電話、または予約システムよりご予約をお願い致します。
			</p>
			<div class="p-index3__btn">
				<a href="" title="" class="c-btn2">
				   ご予約はこちら
			    </a>
				<a href="" title="" class="c-btn3">
				  <span class="c-btn3__text pc-only">ご予約はこちら</span>
				  <span class="c-btn3__text sp-only">お電話でのご予約</span>
					<span class="c-btn3__number">0493ｰ23ｰ8015</span>
			    </a>
			</div>
			<div class="p-index3__text">
				<p>平日11:00～21:00（昼休憩13:00～14:00）／<br>
				土・日・祝10:00～19:00<br>
				定休日 火曜日</p>
			</div>
		</div>
	</section>

	<section class="p-index4" id="instructor">
		<div class="l-container">
			<h2 class="c-title1">
				<img src="assets/image/common/title-instructor.png" alt="" class="pc-only">
				<img src="assets/image/common/title-instructorsp.png" alt="" class="sp-only" width="289" height="34">
			</h2>
			<div class="c-imgtext1">
				<div class="c-imgtext1__img">
					<img src="assets/image/common/img-intructor.png" alt="">
				</div>
				<div class="c-imgtext1__text">
					<h3 class="pc-only">矢島　嘉彦</h3>
					<h3 class="sp-only">講師名が入ります</h3>
					<p>1,000人以上のレッスン経験があり、初心者から上級者までそれぞれの悩みや疑問を解決し指導します。</p>
					<ul class="c-list4">
						<li>公益社団法人日本プロゴルフ協会</li>
						<li>高校生からゴルフを始め、2006年にティーチングプロとして社団法人日本プロゴル
						フ協会の会員となる</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="p-index5" id="shop">
		<div class="l-container">
			<h2 class="c-title1">
				<img src="assets/image/common/title-shop.png" alt="" class="pc-only">
				<img src="assets/image/common/title-shopsp.png" alt="" class="sp-only" width="269" height="34">
			</h2>
			<div class="c-navi">
				<div class="c-navi__item">
						<img src="assets/image/common/img-shop1.png" class="pc-only">
						<img src="assets/image/common/img-shop1.png" class="sp-only" width="136" height="102">
				</div>
				<div class="c-navi__item">
						<img src="assets/image/common/img-shop2.png" class="pc-only">
						<img src="assets/image/common/img-shop2.png" class="sp-only" width="136" height="102">
				</div>
				<div class="c-navi__item">
						<img src="assets/image/common/img-shop3.png" class="pc-only">
						<img src="assets/image/common/img-shop3.png" class="sp-only" width="136" height="102">
				</div>
				<div class="c-navi__item">
						<img src="assets/image/common/img-shop4.png" class="pc-only">
						<img src="assets/image/common/img-shop4.png" class="sp-only" width="136" height="102">
				</div>
				<div class="c-navi__item">
						<img src="assets/image/common/img-shop5.png" class="pc-only">
						<img src="assets/image/common/img-shop5.png" class="sp-only" width="136" height="102">
				</div>
			</div>
		</div>
	</section>

	<section class="p-index6" id="access">
		<div class="l-container">
			<p class="p-index6__title">
				アクセスマップ
			</p>
		</div>
		<div class="p-index6__content">
			<div class="p-index6__map">
				<div id="map"></div>
			</div>
			<div class="p-index6__address">
				<div class="p-index6__text">
					<table>
					<tbody>
						<tr>
							<td><img src="assets/image/common/icon-time.png" alt="" class="pc-only">営業時間</td>
							<td>平日11:00〜21:00(昼休憩13:00〜14:00)<br>土・日・祝10:00〜19:00</td>
						</tr>
						<tr>
							<td ><img src="assets/image/common/icon-date.png" alt="" class="pc-only">定休日</td>
							<td>火曜日</td>
						</tr>
						<tr>
							<td>
								<img src="assets/image/common/icon-lesson.png" alt="" class="pc-only">
								<span>レッスン・フリー<br class="pc-only">利用可能時間</span></td>
							<td>会員・ビジター問わず1時間(準備時間含む)</td>
						</tr>
						<tr>
							<td><img src="assets/image/common/icon-tel.png" alt="" class="pc-only">TEL</td>
							<td>0493-23-8015</td>
						</tr>
						<tr>
							<td ><img src="assets/image/common/icon-address.png" alt="" class="pc-only">住所</td>
							<td>〒355-0028<br>埼玉県東松山市箭弓町1-13-16 安福ビル1F</td>
						</tr>
					</tbody>
				</table>
				</div>
			</div>
		</div>
	</section>

</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>